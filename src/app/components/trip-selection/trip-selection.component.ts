import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinationsService } from '../../services/trip.service'
import { TripInfos, TransportDuration, HousingEnum, ActivitiesEnum } from '../../models/trip';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trip-selection',
  templateUrl: './trip-selection.component.html',
  styleUrls: ['./trip-selection.component.scss'],
})
export class TripSelectionComponent  implements OnInit {

  @Output() setLoader: EventEmitter<any> = new EventEmitter();

  userTripInfos!: TripInfos;
  userTripForm!: FormGroup
  arrayDuration: number[];
  arrayPersons: number[];
  currentFieldShown!: string;
  currentFieldShownNumber: number = 0;
  transportDuration: typeof TransportDuration = TransportDuration;
  housingEnum: typeof HousingEnum = HousingEnum;
  activitiesEnum: typeof ActivitiesEnum = ActivitiesEnum;

  constructor(
    public destinationsService: DestinationsService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.init();
    // this.arrayDuration
    this.arrayDuration = Array.from(new Array(30), (x, i) => i + 1);
    this.arrayPersons = Array.from(new Array(10), (x, i) => i + 1);
  }

  ngOnInit() {

  }

  init() {
    this.userTripInfos = this.destinationsService.userTripInfos;
    this.userTripForm = this.fb.group(this.userTripInfos);
    this.userTripForm.setControl('housingType', this.fb.array([]));
    this.userTripForm.setControl('activities', this.fb.array([]));
    this.currentFieldShownNumber = 0;
    this.currentFieldShown = Object.keys(this.userTripInfos)[this.currentFieldShownNumber];
  }

  handleChangesDuration(event : any) :void {
    // console.log(event.target.id);

    // this.destinationsService.setField()
    // this.userTripInfos.duration = event.target.value;
  }

  onHousingCheckChange(event: any) {
    const formArray: FormArray = this.userTripForm.get('housingType') as FormArray;

    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
  onActivitiesCheckChange(event: any) {
    const formArray: FormArray = this.userTripForm.get('activities') as FormArray;

    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  onSubmit() {
    this.destinationsService.setUserTripInfos(this.userTripForm.value);
    this.destinationsService.selectDestination();
    this.destinationsService.resetTripInfos();
    this.init();

    this.setLoader.emit();
  }

  nextFieldForm() : void {
    if (this.currentFieldShownNumber === Object.keys(this.userTripInfos).length - 1 ) {
      this.onSubmit();
    } else {
      this.currentFieldShownNumber++;
      this.currentFieldShown = Object.keys(this.userTripInfos)[this.currentFieldShownNumber];
    }
  }
  prevFieldForm() : void {
    this.currentFieldShownNumber--;
    this.currentFieldShown = Object.keys(this.userTripInfos)[this.currentFieldShownNumber];
  }

  pinFormatter(value: number) {
    return `€${value}/nuit`;
  }

  ionViewDidLeave() {
    console.log('did leave');

  }
 }
