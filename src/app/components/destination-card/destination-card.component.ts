import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DestinationsService } from 'src/app/services/trip.service';


@Component({
  selector: 'app-destination-card',
  templateUrl: './destination-card.component.html',
  styleUrls: ['./destination-card.component.scss'],
})
export class DestinationCardComponent  implements OnInit {

  @Input() destination: any;
  @Output() onNextSlide = new EventEmitter<any>();
  imageUrlShown: string ='';
  isAlertOpen = false;

  public alertButtons = [
    {
      text: 'Recommencer',
      role: 'confirm',
      handler: () => {
        this.router.navigate(['/tabs/tab1']);
      },
    },
  ];

  constructor(
    private destinationService: DestinationsService,
    private router: Router
  ) { }

  ngOnInit() {
      this.imageUrlShown = this.destination['images'][this.randomIntFromInterval(this.destination['images'].length)];
  }

  nextSlide() {
    this.onNextSlide.emit();
  }
  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }

  randomIntFromInterval(max : number) : number { // min and max included
    return Math.floor(Math.random() * (max))
  }


}
