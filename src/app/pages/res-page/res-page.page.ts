import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DestinationsService } from 'src/app/services/trip.service';
import { IonicSlides } from '@ionic/angular';

@Component({
  selector: 'app-res-page',
  templateUrl: './res-page.page.html',
  styleUrls: ['./res-page.page.scss'],
})
export class ResPagePage implements OnInit {
  @ViewChild('swiper')
  swiperRef: ElementRef | undefined;

  destinationSelections : any[];
  swiperModules = [IonicSlides];
  constructor(
    public destinationsService: DestinationsService,
    private router: Router
  ) {
    this.destinationSelections = this.destinationsService.selectedTrips!;
   }

  ngOnInit() {
    this.destinationSelections = this.destinationsService.selectedTrips!;

    if (!this.destinationSelections) {
      this.router.navigate(['/tabs/tab1']);

    }
    this.destinationSelections.sort(this.isGreaterPoints);
  }

  nextSlide() {
    this.swiperRef!.nativeElement.swiper.slideNext();
  }

  isGreaterPoints(a : any, b : any) : number{
    if(a.points > b.points) {
      return -1;
    }
    if(a.points < b.points) {
      return 1;
    }
    return 0;
  }

}
