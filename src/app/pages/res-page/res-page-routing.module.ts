import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResPagePage } from './res-page.page';

const routes: Routes = [
  {
    path: '',
    component: ResPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResPagePageRoutingModule {}
