import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ResPagePage } from './res-page.page';

describe('ResPagePage', () => {
  let component: ResPagePage;
  let fixture: ComponentFixture<ResPagePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ResPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
