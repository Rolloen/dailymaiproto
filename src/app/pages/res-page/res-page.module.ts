import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { IonicModule } from '@ionic/angular';

import { ResPagePageRoutingModule } from './res-page-routing.module';

import { ResPagePage } from './res-page.page';
import { DestinationCardComponent } from '../../components/destination-card/destination-card.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResPagePageRoutingModule
  ],
  declarations: [ResPagePage, DestinationCardComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ResPagePageModule {}
