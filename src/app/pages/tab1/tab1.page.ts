import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(
    private router: Router,
    private loadingCtrl: LoadingController
  ) {}

  goToResults() {
    this.showLoading()
    setTimeout(() => {
      this.router.navigate(['/tabs/tab1/results']);
    }, 3000);
    // });

  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Recherche de votre meilleure destination',
      duration: 3000,
    });

    loading.present();
  }

}
