import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { ExploreContainerComponentModule } from '../../components/explore-container/explore-container.module';
import { TripSelectionComponent } from '../../components/trip-selection/trip-selection.component';
import { IsIntPipe } from '../../services/pipes/is-int.pipe'

import { Tab1PageRoutingModule } from './tab1-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab1PageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    Tab1Page,
    TripSelectionComponent,
    IsIntPipe
  ]
})
export class Tab1PageModule {}
