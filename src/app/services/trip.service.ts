import { Injectable } from '@angular/core';
import { TripInfos, TransportDuration, HousingEnum, ActivitiesEnum } from '../models/trip';
import { DESTINATIONS } from "../models/destination";
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DestinationsService {

  userTripInfos!: TripInfos;
  selectedTrips? : any[];
  tripsLoaded = new Subject();

  constructor() {
    this.resetTripInfos();
  }

  setField(fieldName: string, value: any): void {
    this.userTripInfos = { ...this.userTripInfos, [fieldName]: value};
  }
  setUserTripInfos(value: TripInfos): void {
    console.log('value', value);
    this.userTripInfos = value;
  }

  resetTripInfos() {
    this.userTripInfos = {
      duration: 7,
      nbPersons: 0,
      budget: {
        lower: 30,
        upper: 80
      },
      transportDuration: TransportDuration["3 à 5 heures"],
      housingType: [HousingEnum['g']],
      activities: [ActivitiesEnum['g']],
      animal: false,
      eco: false
    }
  }

  selectDestination() : any {
    let res : any[] = [];
    for (const dest of DESTINATIONS) {
      let destCopy = {...dest};
      //budget check
      if (dest.budget > 0 && this.userTripInfos.budget.upper > dest.budget ) {
        destCopy.points += 5;
      } else if (dest.budget === 0 ){
        destCopy.points += 1;
      }

      //duration check
      if(dest.duration.length === 2) {
        if (this.userTripInfos.duration <= dest.duration[0]){
          destCopy.points += 4;
        }
      } else if (dest.duration.length > 2) {
        if (this.userTripInfos.duration >= dest.duration[0] && this.userTripInfos.duration <= dest.duration[1]) {
          destCopy.points += 4;
        }
      }

      //transport check
      if (dest.transportDuration[1] <= this.userTripInfos.transportDuration) {
        destCopy.points += 4;
      }

      // housing check
      if (this.userTripInfos.housingType.length > 0 ) {
        for (const userHousing of this.userTripInfos.housingType) {
          if (userHousing === HousingEnum['a']) {
            continue;
          }

          if (destCopy.housingType.includes(userHousing)) {
            destCopy.points += 1;
          }
        }
      }

      //activities check
      if (this.userTripInfos.activities.length > 0 ) {
        for (const acti of this.userTripInfos.activities) {
          if (acti === ActivitiesEnum['a']) {
            continue;
          }
          if (destCopy.activities.includes(acti)) {
            destCopy.points += 1;
          }
        }
      }

      //final
      res.push(destCopy);
    }
    this.selectedTrips = res;
    this.tripsLoaded.next(true);
  }
}
