import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isInt'
})
export class IsIntPipe implements PipeTransform {


  transform(value: any): any {
    return value.length <= 2 ;
  }

}
