export interface TripInfos {
  duration: number;
  nbPersons: number;
  budget: Budget;
  transportDuration: number;
  housingType: HousingEnum[];
  activities: ActivitiesEnum[];
  animal: boolean;
  eco: boolean;
}

// export enum TransportDuration {
//   '0noLimit' = 'Peu importe',
//   '12hours' = '- de 2 heures',
//   '23-5hours' = '3 à 5 heures',
//   '36-9hours' = '6 à 9 heures',
//   '4+10hours' = '+ de 10 heures'
// }
export enum TransportDuration {
  'Peu importe' = 0,
  '- de 2 heures' = 2,
  '3 à 5 heures' = 5,
  '6 à 9 heures' = 9,
   '+ de 10 heures' = 10
}
export enum HousingEnum {
  'a' = 'Peu importe',
  'b' = 'Camping',
  'c' = 'Airbnb',
  'd' = 'Hotel',
  'e' = 'Auberges',
  'f' = 'Résidences',
  'g' = 'Autres',
}
export enum ActivitiesEnum {
  'a' = 'Peu importe',
  'b' = 'Tourisme',
  'c' = 'Nature',
  'd' = 'Sport',
  'e' = 'Culture',
  'f' = 'Farniente',
  'g' = 'Autre',
}

export interface Budget {
  lower: number;
  upper: number;
}
